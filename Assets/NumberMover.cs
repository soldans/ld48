using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberMover : MonoBehaviour
{
    LookingGlassMover lgmscript;
    float timeLeftBeforeSwitchingDirection;
    Vector3 moveDir;

    private int value = 0;
    public int Value
    {
        get { return value; }
        set
        {
            this.value = value;
            gameObject.GetComponent<TextMesh>().text = value.ToString();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        moveDir = new Vector3(Random.Range(-5.0f, 5.0f), Random.Range(-5.0f, 5.0f), 0);
        moveDir.Normalize();
        timeLeftBeforeSwitchingDirection = 60.0f;
        lgmscript = GameObject.Find("LookingGlass").GetComponent<LookingGlassMover>();
    }

    // Update is called once per frame
    void Update()
    {
        timeLeftBeforeSwitchingDirection -= Time.deltaTime;
        if (timeLeftBeforeSwitchingDirection <= 0.0f) {
            moveDir.x = Random.Range(-2.5f, 2.5f);
            moveDir.y = Random.Range(-2.5f, 2.5f);
            moveDir.Normalize();
            timeLeftBeforeSwitchingDirection = 60.0f;
        }
        gameObject.transform.Translate(moveDir * Time.deltaTime);

        Vector3 limitedpos = gameObject.transform.position;
        
        //Random.value
        if (gameObject.transform.position.x > 4.8f) {
            limitedpos.x = 4.8f;
            moveDir.x = -moveDir.x;
        }
            
        if (gameObject.transform.position.x < -4.8f) {
            limitedpos.x = -4.8f;
            moveDir.x = -moveDir.x;
        }
        if (gameObject.transform.position.y > 3.5f) {
            limitedpos.y = 3.5f;
            moveDir.y = -moveDir.y;
        }
        if (gameObject.transform.position.y < -4.8f) {
            limitedpos.y = -4.8f;
            moveDir.y = -moveDir.y;
        }
        float closestDistSq = 1000000;
        lgmscript.getTextObjectClosestToPosition(gameObject.transform.position, out closestDistSq, gameObject.transform);
        if (closestDistSq < 0.5f) {
            moveDir = -moveDir;
        }

        gameObject.transform.position = limitedpos;
    }
}
