using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookingGlassMover : MonoBehaviour
{
    public float mouseSpeed = 0.25f;
    public GameObject textPrefab;
    private int numberOfTexts = 2;
    public int numberOfTextsStart = 2;

    private GameObject textHolder;
    
    private GameObject timeBar;
    private Vector3 timeBarScale;

    private GameObject lookingGlassHole;
    private Vector3 lookingGlassHoleScale;
    private TextMesh gameStatsTextMesh;

    private int score = 0;
    private int highScore = 0;
    private int currentLevel = 1;
    private int highestLevel = 1;
    private float timeLeftOfRound = 60;
    private float roundTime = 60;
    public float startRoundTime = 60;

    enum GameStates
    {
        Start = 0,
        Playing = 1,
        GameOver = 2
    }

    private GameStates currentGameState;
    

    // Start is called before the first frame update
    void Start() {
        Hashtable myHashtable = new Hashtable();
        myHashtable["value"] = 42;

        Debug.Log("type: " + myHashtable["value"].GetType().ToString());
        timeBarScale = new Vector3(1.0f, 1.0f, 0.05f);
        lookingGlassHoleScale = new Vector3(4.0f, 2.0f, 4.0f);
        currentGameState = GameStates.Start;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        textHolder = GameObject.Find("TextHolder");
        lookingGlassHole = GameObject.Find("LookingGlassHole");
        timeBar = GameObject.Find("TimeBar");
        gameStatsTextMesh = GameObject.Find("GameStatsText").GetComponent<TextMesh>();
    }

    int getHighestNumber() {
        int highestNumber = -1000000;

        int numChildren = textHolder.transform.childCount;
        if (numChildren == 0)
        {
            return -1;
        }
        for (int i = 0; i < numChildren; i++) {
            GameObject go = textHolder.transform.GetChild(i).gameObject;
            highestNumber = Mathf.Max(highestNumber, go.GetComponent<NumberMover>().Value);
        }
        return highestNumber;
    }

    public GameObject getTextObjectClosestToPosition(Vector3 position, out float closestDistSq, Transform exlude = null)
    {
        GameObject closestTextObject = null;
        closestDistSq = 1000000.0f;

        // TODO loop through children of textHolder

        //int val = textHolder.transform.GetChild(1).gameObject.GetComponent<IntValue>().Value;
        int numChildren = textHolder.transform.childCount;
        if(numChildren == 0) {
            return null;
        }
        for (int i = 0; i < numChildren; i++) {
            if(exlude != textHolder.transform.GetChild(i)) {
                float distSq = new Vector2(position.x - textHolder.transform.GetChild(i).position.x, position.y - textHolder.transform.GetChild(i).position.y).sqrMagnitude;
                if (distSq < closestDistSq) {
                    closestDistSq = distSq;
                    closestTextObject = textHolder.transform.GetChild(i).gameObject;
                }
            }
        }

        return closestTextObject;
    }

    private void Init()
    {
        Debug.Log("Init");

        gameObject.transform.position = new Vector3(0, 0, -2);

        numberOfTexts = numberOfTextsStart;
        score = 0;
        currentLevel = 0;
        timeLeftOfRound = roundTime = startRoundTime;

        for (int i = 0; i < textHolder.transform.childCount; i++)
        {
            GameObject go = textHolder.transform.GetChild(i).gameObject;
            Destroy(go);
        }

        CreateNumbers();
        currentGameState = GameStates.Playing;
    }

    private void CreateNumbers()
    {
        Debug.Log("CreateNumbers");
        numberOfTexts = Mathf.Min(25, numberOfTexts); // CLAMP AT 25
        // TODO: break out this to method to be re-used;
        for (int i = 0; i < numberOfTexts; i++)
        {
            int val = Random.Range(0, 99);

            //numberStats[val]++;
            float distSq = 0;
            Vector3 pos = Vector3.zero;
            while (distSq < 2.0f) {
                pos = new Vector3(Random.Range(-4.8f, 4.8f), Random.Range(-4.8f, 3.5f));
                getTextObjectClosestToPosition(pos, out distSq);
            }           
            
            //Debug.Log(pos);

            GameObject go = Instantiate(textPrefab, pos, Quaternion.identity, textHolder.transform);

            go.GetComponent<NumberMover>().Value = val;
        }
    }

    // Update is called once per frame
    void Update()
    {
        switch (currentGameState)
        {
            case GameStates.GameOver:
                gameStatsTextMesh.text = "GAME OVER - Press space to start new game\n You reached level: " + currentLevel + " with a score of " + score + "\nHighscore: " + highScore + " , Highest level: " + highestLevel;
                if (Input.GetKeyDown(KeyCode.Space))
                    Init();
                break;
            case GameStates.Start:
                gameStatsTextMesh.text = "Search for numbers between 0 and 99 by moving mouse\nClick at numbers in descending order.\nPress Space to start new game\nPress Esc to quit.";
                if (Input.GetKeyDown(KeyCode.Space))
                    Init();
                break;
            case GameStates.Playing:
                HandleMovement();
                UpdateTimeBar();

                if (Input.GetMouseButtonDown(0))
                {
                    float sqDist;
                    GameObject go = getTextObjectClosestToPosition(gameObject.transform.position, out sqDist);
                    if (sqDist < .25f) {
                        if (go.GetComponent<NumberMover>().Value == getHighestNumber()) {
                            
                            Destroy(go); // mark for destruction

                            score += 5;
                            // childCount == 1 before the last one has been;
                            if (textHolder.transform.childCount == 1) {
                                score += (int)(timeLeftOfRound / roundTime * 100.0f);
                                currentLevel++;
                                numberOfTexts++;
                                roundTime -= 0.5f;
                                timeLeftOfRound = roundTime;

                                lookingGlassHoleScale.x = Mathf.Max(1.4f, lookingGlassHoleScale.x - 0.05f);
                                lookingGlassHoleScale.z = lookingGlassHoleScale.x;
                                lookingGlassHole.transform.localScale = lookingGlassHoleScale;

                                CreateNumbers();
                            }
                        } else {
                            score -= 10;
                            timeLeftOfRound -= 5.0f;
                        }
                                                
                    }
                    
                    // EXTRAS: time limited bonus life / health packs?
                }
                gameStatsTextMesh.text = "Level: " + currentLevel + " score: " + score + " left: " + textHolder.transform.childCount + " of " + numberOfTexts + "\nHighscore: " + highScore + " , Highest level: " + highestLevel;
                break;
        }

        if (Input.GetKey(KeyCode.Escape))
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            Application.Quit();
        }
    }

    private void UpdateTimeBar()
    {
        timeLeftOfRound -= Time.deltaTime;
        if (timeLeftOfRound <= 0.0f)
        {
            if (score > highScore)
                highScore = score;
            if (currentLevel > highestLevel)
                highestLevel = currentLevel;
            
            currentGameState = GameStates.GameOver;
        }
        timeBarScale.x = this.timeLeftOfRound / roundTime;
        timeBarScale.x = Mathf.Max(0,timeBarScale.x);
        
        timeBar.transform.localScale = timeBarScale;
        
        timeBar.GetComponent<Renderer>().material.color = new Color(1.0f - timeBarScale.x, 0, timeBarScale.x * timeBarScale.x); // REDDER WHEN TIME LOW 1/timeBarScale.x

        // TODO: implement. Need Progressbar thingy!
        // if < 0 score--;
        // health--;
        // 3 lifes?
    }

    private void HandleMovement()
    {
        gameObject.transform.Translate(Input.GetAxis("Mouse X") * mouseSpeed, Input.GetAxis("Mouse Y") * mouseSpeed, 0);
        Vector3 limitedpos = gameObject.transform.position;
        if (gameObject.transform.position.x > 5)
            limitedpos.x = 5;
        if (gameObject.transform.position.x < -5)
            limitedpos.x = -5;
        if (gameObject.transform.position.y > 4)
            limitedpos.y = 4;
        if (gameObject.transform.position.y < -5)
            limitedpos.y = -5;

        gameObject.transform.position = limitedpos;
    }
}
