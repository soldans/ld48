using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class IntValue : MonoBehaviour
{
    private int value = 0;
    public int Value
    {
        get { return value; }
        set
        {
            this.value = value;
            gameObject.GetComponent<TextMesh>().text = value.ToString();
        }
    }
}
